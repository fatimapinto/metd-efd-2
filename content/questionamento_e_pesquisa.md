---
title: Atividades de questionamento e pesquisa
linkTitle: Questionamento e pesquisa
# subtitle: << subtitle >>
tags: []
menu:
  main:
    parent: "praticas"
weight: 22
---


Na era digital, na sociedade do Google em que o
conhecimento parece estar à distância de um simples clique deixa de
fazer sentido que o processo educativo se baseia apenas na
transmissão de conteúdos pelo professor. Conforme referido por
Dietrich& Baly (2014, p. 11) “o professor mais do que ser o
centro da instrução deve cada vez mais tornar-se facilitador do
processo de aprendizagem recorrendo a atividades de questionamento e
de pesquisa (*inquiry - based*)
e utilizando a tecnologia como um veículo para acesso à
informação”.

Ou
seja, mais do que utilizar tecnologias na sala de aula é também
necessário promover uma aprendizagem diferente na qual o aluno
participa ativamente na construção do seu próprio conhecimento.
Uma excelente forma de envolver o aluno na aprendizagem e estimular a
sua curiosidade é através da formulação de questões que os
alunos têm que investigar.

Um
exemplo prático e como forma de iniciar a aprendizagem de um tópico
os alunos, em grupo, terão que escolher uma questão que esteja
relacionado com o tópico e que os alunos gostassem de saber mais. Em
turma, com a orientação do professor, os alunos escolhem a
pergunta(s) que se se revela mais adequada. Os alunos poderão depois
fazer uma pesquisa na internet para responder à questão. As
possíveis respostas deverão depois ser apresentadas e debatidas na
sala de aula.
