---
title: Nativos digitais e Imigrantes digitais
linkTitle: Nativos e Imigrantes
# subtitle: << subtitle >>
tags: []
menu: 
  main:
weight: 10
---


As
crianças e jovens que frequentam a escola de hoje vivem num mundo
tecnológico e digital. Para eles não existe um antes e um depois da
Internet. O mundo sempre se apresentou ligado e nunca desconectado da
Internet ou dos *smartphones*. São por isso designados com frequência “nativos digitais”. Em
oposição aos “nativos digitais” existem os “imigrantes
digitais” ou seja, todos os outros que, ao contrário dos nativos
digitais não nasceram num mundo digital mas que em algum momento da
sua vida adotaram as novas tecnologias e alguns dos aspetos que
caracterizam o mundo digital ( Prensky, 2001a, p. 1). O “imigrante
digital” necessita de aprender uma nova linguagem – a linguagem
digital – a qual é intrínseca ao “nativo digital”, fluente na
linguagem digital dos computadores, vídeo jogos e da Internet
(Prensky, 2006).

Esta
exposição constante às novas tecnologias e ao digital trouxe novas
competências à geração de ‘nativos digitais’. Marc
Prensky no artigo *Digital natives, digital immigrants, Part II: Do
they really think differently?* (2001b) sugere mesmo que *“Digital
Natives’ brains are likely physically different as a result of the
digital input they received growing up”*.
As crianças e jovens de hoje estão habituados ao
conceito de *multi-tasking*
(fazer várias coisas ao mesmo tempo), desenvolveram uma linguagem
gráfica e visual e preferem ainda a aleatoriedade do hipertexto ao
processo rígido e estruturado de um livro de texto. Por esta razão,
argumenta o autor no mesmo artigo que os alunos estão “bored”
(aborrecidos) com a educação de hoje e que,

> “the many skills that new technologies have actually enhanced (e.g.,
parallel processing, graphics awareness, and random access)—which
have profound implications for their learning—are almost totally
ignored by educators.” (2001b, p. 5).

Este
é um desafio na educação de hoje: impulsionar as competências
que os ‘nativos digitais’ evidenciam e estimular outras que se
revelam muito importantes no contexto da sociedade atual como o
pensamento crítico e a capacidade de reflexão.

**Referências Bibliográficas**

Prensky,
M. (2001a). *Digital
Natives, Digital Immigrants*. MCB
University Press, 9(5), 1-6, disponível em
[https://www.marcprensky.com/writing/Prensky%20-%20Digital%20Natives,%20Digital%20Immigrants%20-%20Part1.pdf](https://www.marcprensky.com/writing/Prensky%20-%20Digital%20Natives,%20Digital%20Immigrants%20-%20Part1.pdf)
[acedido em 18-10-2020].

Prensky, M. (2001b).
*Digital
natives, digital immigrants, Part II: Do they really think
differently?* 

Prensky,
M. (2006). *Listen
to the Natives. Educational Leadership*,
63(4), 8-13, disponível em
[http://www.ascd.org/ASCD/pdf/journals/ed_lead/el200512_prensky.pdf](http://www.ascd.org/ASCD/pdf/journals/ed_lead/el200512_prensky.pdf)[acedido
em 18-10-2020].
