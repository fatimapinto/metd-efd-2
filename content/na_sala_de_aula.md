---
title: Tecnologias na sala de aula
linkTitle: Na sala de aula
# subtitle: << subtitle >>
tags: []
menu:
  main:
    parent: "praticas"
weight: 21
---

**Jogos digitais**

A
utilização de jogos na sala de aula pode despertar o interesse dos
alunos que estando bastante familiarizados com esta realidade poderão
mais facilmente se sentir motivados para aprender. O facto de se
recorrer a jogos não significa que os conteúdos não possam sérios.
Um simples quiz
pode motivar o aluno no processo de aprendizagem se este encontrar
aliciantes que normalmente encontra num jogo fora do contexto da sala
de aula (prémio; competição). O quiz
poderá ainda ter a vantagem de o aluno receber feedback imediato.

**Mapas concetuais online**

Os
mapas concetuais são representações gráficas que indicam relações
entre conceitos ligados por palavras. Sendo uma representação
gráfica os alunos ‘digitais’ mais facilmente poderão visualizar
relações e significados e extrapolar conhecimento dos mesmos. Tanto
a análise como a criação de mapas concetuais poderá estimular as
competências dos alunos. A criação de mapas online através de
ferramentas colaborativas permite ainda estimular competências de
colaboração e cooperação entre os alunos.

**Skype**

Ferramentas
como o Skype permitem trazer o mundo real (profissionais;
especialistas; histórias reais) para a sala de aula e esta pode ser
uma forma de motivar os alunos no processo de aprendizagem.
Iniciativas como o *Espaço vai à escola*
na qual cientistas e engenheiros que trabalham em áreas ligadas ao
espaço dão palestras para as escolas, a convite do [ESERO
Portugal](https://www.esero.pt/), são ainda mais fáceis recorrendo ao formato online
através de plataformas como Skype. O professor poderá antes da
sessão com o especialista trabalhar o tema com os alunos e os alunos
em grupo poderão preparar algumas questões para o palestrante. As
questões poderão ser enviadas antecipadamente por e-mail ou poderão
ser colocadas durante o próprio evento através do chat.

**Youtube**

A
plataforma do Youtube permite não só a visualização de vídeos
mas também o upload de vídeos. Hoje em dia muitos jovens têm como
ídolos *Youtubers*
que partilham vídeos sobre os mais diversos tópicos e que são
seguidos nas suas redes sociais por milhares de utilizadores. Esta
poderá ser por isso uma excelente ferramenta para motivar os alunos.
Em grupos, os alunos poderão ter como trabalho a realização de um
vídeo ou mini tutorial para explicar determinado tópico e que
deverão depois partilhar no Youtube. A realização de vídeos é
também uma excelente forma de apresentação de trabalhos não só
para a turma mas para uma comunidade mais alargada.

**Goggle
arts and culture**

Através
do site [Google
Arts & Culture](https://artsandculture.google.com/partner) os alunos podem ter acesso a mais de 2000
museus espalhados pelo mundo. Como trabalho de casa o professor
poderá sugerir que os alunos naveguem no site e escolham uma obra de
arte (de acordo com a faixa etária o professor poderá sugerir
alguns museus). Os alunos deverão depois fazer uma sucinta
apresentação utilizando, por exemplo, o Power Point, na qual
apresentam uma espécie de bilhete de identidade da obra de arte
(Título; Autor; período e estilo que representa; museu onde se
encontra). Os alunos partilham a obra de arte escolhida (poderão
optar por uma das ferramentas disponíveis para partilha como por
exemplo o email ou Google Classroom) e enquanto turma desenvolvem a
sua própria exposição com as obras de arte selecionadas.
