---
title: Connectivismo
# subtitle: << subtitle >>
tags: []
menu:
  main
weight: 1
---


George Siemens no
texto *Connectivism: A Learning Theory for the Digital Age* (2004)
define o conectivismo como:

> “Connectivism is the integration of principles explored by
chaos, network, and complexity and self-organization theories.
Learning is a process that occurs within
nebulous environments of shifting core elements – not entirely
under the control of the individual. Learning (defined
as actionable knowledge) can reside outside of ourselves (within an
organization or a database), is focused on connecting specialized
information sets, and the connections that enable us to learn more
are more important than our current state of knowing.”

O
conectivismo surge como uma nova teoria de aprendizagem que engloba
as características e desafios da era digital e da sociedade de
informação do século XXI. O acesso à informação, a rapidez com
que a informação é transmitida e também renovada, assim como o
acesso facilitado às novas tecnologias moldaram por completo a forma
como comunicamos, a forma como interagimos em sociedade e também a
forma como aprendemos. É neste contexto que nasce o conectivismo.

O
conectivismo surge como resposta a uma sociedade na qual a informação
é abundante e cresce exponencialmente, de uma forma caótica,
complexa e imprevisível. Por isso, mais importante do que o saber é
a capacidade de aprender, de sintetizar, de reconhecer conexões e
dar significado à informação. O conhecimento deixa de ser algo
individual, um produto, para ser um processo que flui entre redes de
conexões e que é ampliado pela comunidade e sociedade em rede na
qual nos situamos.

**Referências bibliográficas**

Siemens,
G. (2004). *Connectivism: A Learning
Theory for the Digital Age*. International Journal
of Instructional Technology and Distance Learning, 2(1). Disponível
em [http://www.itdl.org/Journal/Jan_05/article01.htm](http://www.itdl.org/Journal/Jan_05/article01.htm) [acedido
em 17-10-2020].

