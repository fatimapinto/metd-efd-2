## Objetivo

O
objetivo deste recurso educativo digital é apresentar o conceito de
conectivismo enquanto
perspectiva teórica acerca da aprendizagem humana na sociedade do
século XXI. Serão ainda apresentas sugestões de práticas que os
educadores atuais podem adotar para impulsionar as competências
das novas gerações, os “nativos digitais”.

Este
recurso foi desenvolvido no âmbito da disciplina de e-learning e
formação à distância do 1ºano
do mestrado em Educação e Tecnologias digitais do Instituto de
Educação da Universidade de Lisboa.

[Connectivismo](connectivismo)
